/**
 * Módulo de enrutamiento para las solicitudes relacionadas con los empresarios.
 * 
 * Este módulo define las rutas para manejar las solicitudes HTTP relacionadas con la creación, obtención, actualización y eliminación de empresarios.
 * 
 * @module RutasEmpresarios
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024
 */

// Importa el enrutador de Express
import { Router } from "express";

// Importa los controladores para las operaciones relacionadas con los empresarios
import { crearEmpresario, obtenerEmpresarios, obtenerEmpresariosID, eliminarEmpresarios, actualizarEmpresarios } from "../controllers/empresario.controller.js";

// Crea un enrutador de Express
const router = Router();

// Rutas para las diferentes operaciones CRUD de empresarios
router.post('/crearEmpresario', crearEmpresario); // Ruta para crear un nuevo empresario
router.get('/obtenerEmpresario', obtenerEmpresarios); // Ruta para obtener todos los empresarios
router.get('/obtenerEmpresario/:id', obtenerEmpresariosID); // Ruta para obtener un empresario por su ID
router.delete('/eliminarEmpresario/:id', eliminarEmpresarios); // Ruta para eliminar un empresario por su ID
router.patch('/actualizarEmpresario/:id', actualizarEmpresarios); // Ruta para actualizar un empresario por su ID

// Exporta el enrutador
export default router;
