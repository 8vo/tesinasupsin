/**
 * Módulo de enrutamiento para las solicitudes relacionadas con usuarios.
 * 
 * Este módulo define las rutas para manejar las solicitudes HTTP relacionadas con la creación, obtención, actualización y eliminación de usuarios.
 * 
 * @module RutasUsuarios
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024
 */

// Importa el enrutador de Express
import { Router } from "express";

// Importa los controladores para las operaciones relacionadas con usuarios
import { actualizarUsuario, crearUsuario, eliminarUsuario, obtenerUsuarios, obtenerUsuariosID, ping } from "../controllers/index.controller.js";

// Crea un enrutador de Express
const router = Router();

// Rutas para las diferentes operaciones CRUD de usuarios
router.get('/ping', ping); // Ruta para comprobar si el servidor está en línea
router.post('/crearUsuario', crearUsuario); // Ruta para crear un nuevo usuario
router.get('/obtenerUsuarios', obtenerUsuarios); // Ruta para obtener todos los usuarios
router.get('/obtenerUsuarios/:id', obtenerUsuariosID); // Ruta para obtener un usuario por su ID
router.delete('/eliminarUsuario/:id', eliminarUsuario); // Ruta para eliminar un usuario por su ID
router.patch('/actualizarUsuario/:id', actualizarUsuario); // Ruta para actualizar un usuario por su ID

// Exporta el enrutador
export default router;
