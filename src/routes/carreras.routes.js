/**
 * Módulo de enrutamiento para las solicitudes relacionadas con las Carreras.
 * 
 * Este módulo define las rutas para manejar las solicitudes HTTP relacionadas con la creación, obtención, actualización y eliminación de Carreras.
 * 
 * @module RutasCarreras
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024
 * 
 */

// Importa el enrutador de Express
import { Router } from "express";

// Importa los controladores para las operaciones relacionadas con los Carreras
import { crearcarreras, obtenercarreras, obtenercarrerasID, eliminarcarreras, actualizarcarreras } from "../controllers/carreras.controller.js";

// Crea un enrutador de Express
const router = Router();

// Rutas para las diferentes operaciones CRUD de Carreras
router.post('/crearCarreras', crearcarreras); // Ruta para crear un nuevo Carreras
router.get('/obtenerCarreras', obtenercarreras); // Ruta para obtener todos los Carreras
router.get('/obtenerCarreras/:id', obtenercarrerasID); // Ruta para obtener un Carreras por su ID
router.delete('/eliminarCarreras/:id', eliminarcarreras); // Ruta para eliminar un Carreras por su ID
router.patch('/actualizarCarreras/:id', actualizarcarreras); // Ruta para actualizar un Carreras por su ID

//Rutas para las diferentes operaciones CRUD de los detalles de las Carrerass

// Exporta el enrutador
export default router;
