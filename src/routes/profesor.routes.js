/**
 * Módulo de enrutamiento para las solicitudes relacionadas con las profesores.
 * 
 * Este módulo define las rutas para manejar las solicitudes HTTP relacionadas con la creación, obtención, actualización y eliminación de profesores.
 * 
 * @module Rutasprofesores
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024
 * 
 */

// Importa el enrutador de Express
import { Router } from "express";

// Importa los controladores para las operaciones relacionadas con los profesores
import { crearprofesores, obtenerprofesores, obtenerprofesoresID, eliminarprofesores, actualizarprofesores } from "../controllers/profesor.controller.js";

// Crea un enrutador de Express
const router = Router();

// Rutas para las diferentes operaciones CRUD de profesores
router.post('/crearprofesores', crearprofesores); // Ruta para crear un nuevo profesores
router.get('/obtenerprofesores', obtenerprofesores); // Ruta para obtener todos los profesores
router.get('/obtenerprofesores/:id', obtenerprofesoresID); // Ruta para obtener un profesores por su ID
router.delete('/eliminarprofesores/:id', eliminarprofesores); // Ruta para eliminar un profesores por su ID
router.patch('/actualizarprofesores/:id', actualizarprofesores); // Ruta para actualizar un profesores por su ID

//Rutas para las diferentes operaciones CRUD de los detalles de las profesoress

// Exporta el enrutador
export default router;
