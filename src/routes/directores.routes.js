/**
 * Módulo de enrutamiento para las solicitudes relacionadas con las directores.
 * 
 * Este módulo define las rutas para manejar las solicitudes HTTP relacionadas con la creación, obtención, actualización y eliminación de directores.
 * 
 * @module Rutasdirectores
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024
 * 
 */

// Importa el enrutador de Express
import { Router } from "express";

// Importa los controladores para las operaciones relacionadas con los directores
import { creardirectores, obtenerdirectores, obtenerdirectoresID, eliminardirectores, actualizardirectores } from "../controllers/directores.controller.js";

// Crea un enrutador de Express
const router = Router();

// Rutas para las diferentes operaciones CRUD de directores
router.post('/creardirectores', creardirectores); // Ruta para crear un nuevo directores
router.get('/obtenerdirectores', obtenerdirectores); // Ruta para obtener todos los directores
router.get('/obtenerdirectores/:id', obtenerdirectoresID); // Ruta para obtener un directores por su ID
router.delete('/eliminardirectores/:id', eliminardirectores); // Ruta para eliminar un directores por su ID
router.patch('/actualizardirectores/:id', actualizardirectores); // Ruta para actualizar un directores por su ID

//Rutas para las diferentes operaciones CRUD de los detalles de las directoress

// Exporta el enrutador
export default router;
