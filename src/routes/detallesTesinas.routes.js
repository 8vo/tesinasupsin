/**
 * Módulo de enrutamiento para las solicitudes relacionadas con los Tesina.
 * 
 * Este módulo define las rutas para manejar las solicitudes HTTP relacionadas con la creación, obtención, actualización y eliminación de Tesina.
 * 
 * @module RutasTesina
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024
 * 
 */

// Importa el enrutador de Express
import { Router } from "express";

// Importa los controladores para las operaciones relacionadas con los Tesina
import { crearDetalleTesinas, obtenerDetalleTesinas, obtenerDetalleTesinasID, eliminarDetalleTesinas, actualizarDetalleTesinas} from "../controllers/detallesTesinas.controller.js";

// Crea un enrutador de Express
const router = Router();

// Rutas para las diferentes operaciones CRUD de Tesina
router.post('/crearDetalleTesinas', crearDetalleTesinas); // Ruta para crear un nuevo Tesina
router.get('/obtenerDetalleTesinas', obtenerDetalleTesinas); // Ruta para obtener todos los Tesina
router.get('/obtenerDetalleTesinas/:id', obtenerDetalleTesinasID); // Ruta para obtener un Tesina por su ID
router.delete('/eliminarDetalleTesinas/:id', eliminarDetalleTesinas); // Ruta para eliminar un Tesina por su ID
router.patch('/actualizarDetalleTesinas/:id', actualizarDetalleTesinas); // Ruta para actualizar un Tesina por su ID

//Rutas para las diferentes operaciones CRUD de los detalles de las Tesinas

// Exporta el enrutador
export default router;
