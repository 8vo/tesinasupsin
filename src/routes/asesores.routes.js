/**
 * Módulo de enrutamiento para las solicitudes relacionadas con las asesores.
 * 
 * Este módulo define las rutas para manejar las solicitudes HTTP relacionadas con la creación, obtención, actualización y eliminación de asesores.
 * 
 * @module Rutasasesores
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024
 * 
 */

// Importa el enrutador de Express
import { Router } from "express";

// Importa los controladores para las operaciones relacionadas con los asesores
import { crearasesores, obtenerasesores, obtenerasesoresID, eliminarasesores, actualizarasesores } from "../controllers/asesores.controller.js";

// Crea un enrutador de Express
const router = Router();

// Rutas para las diferentes operaciones CRUD de asesores
router.post('/crearasesores', crearasesores); // Ruta para crear un nuevo asesores
router.get('/obtenerasesores', obtenerasesores); // Ruta para obtener todos los asesores
router.get('/obtenerasesores/:id', obtenerasesoresID); // Ruta para obtener un asesores por su ID
router.delete('/eliminarasesores/:id', eliminarasesores); // Ruta para eliminar un asesores por su ID
router.patch('/actualizarasesores/:id', actualizarasesores); // Ruta para actualizar un asesores por su ID

//Rutas para las diferentes operaciones CRUD de los detalles de las asesoress

// Exporta el enrutador
export default router;
