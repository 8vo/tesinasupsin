/**
 * Módulo de enrutamiento para las solicitudes relacionadas con los alumnos.
 * 
 * Este módulo define las rutas para manejar las solicitudes HTTP relacionadas con la creación, obtención, actualización y eliminación de alumnos.
 * 
 * @module RutasAlumnos
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024
 * 
 */

// Importa el enrutador de Express
import { Router } from "express";

// Importa los controladores para las operaciones relacionadas con los alumnos
import { crearAlumno, obtenerAlumnos, obtenerAlumnosID, eliminarAlumnos, actualizarAlumnos } from "../controllers/alumno.controller.js";

// Crea un enrutador de Express
const router = Router();

// Rutas para las diferentes operaciones CRUD de alumnos
router.post('/crearAlumno', crearAlumno); // Ruta para crear un nuevo alumno
router.get('/obtenerAlumnos', obtenerAlumnos); // Ruta para obtener todos los alumnos
router.get('/obtenerAlumno/:id', obtenerAlumnosID); // Ruta para obtener un alumno por su ID
router.delete('/eliminarAlumno/:id', eliminarAlumnos); // Ruta para eliminar un alumno por su ID
router.patch('/actualizarAlumno/:id', actualizarAlumnos); // Ruta para actualizar un alumno por su ID

// Exporta el enrutador
export default router;
