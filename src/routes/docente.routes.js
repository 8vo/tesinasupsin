/**
 * Módulo de enrutamiento para las solicitudes relacionadas con los docentes.
 * 
 * Este módulo define las rutas para manejar las solicitudes HTTP relacionadas con la creación, obtención, actualización y eliminación de docentes.
 * 
 * @module RutasDocentes
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024
 * 
 */

// Importa el enrutador de Express
import { Router } from "express";

// Importa los controladores para las operaciones relacionadas con los docentes
import { crearDocente, obtenerDocentes, obtenerDocenteID, actualizarDocente, eliminarDocente } from "../controllers/docente.controller.js";

// Crea un enrutador de Express
const router = Router();

// Rutas para las diferentes operaciones CRUD de docentes
router.post('/crearDocente', crearDocente); // Ruta para crear un nuevo docente
router.get('/obtenerDocentes', obtenerDocentes); // Ruta para obtener todos los docentes
router.get('/obtenerDocentes/:id', obtenerDocenteID); // Ruta para obtener un docente por su ID
router.delete('/eliminarDocentes/:id', eliminarDocente); // Ruta para eliminar un docente por su ID
router.patch('/actualizarDocentes/:id', actualizarDocente); // Ruta para actualizar un docente por su ID

// Exporta el enrutador
export default router;
