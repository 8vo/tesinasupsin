/**
 * Módulo de enrutamiento para las solicitudes relacionadas con los Tesina.
 * 
 * Este módulo define las rutas para manejar las solicitudes HTTP relacionadas con la creación, obtención, actualización y eliminación de Tesina.
 * 
 * @module RutasTesina
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024
 * 
 */

// Importa el enrutador de Express
import { Router } from "express";

// Importa los controladores para las operaciones relacionadas con los Tesina
import { crearTesina, obtenerTesinas, obtenerTesinaID, eliminarTesina, actualizarTesina } from "../controllers/tesinas.controller.js";

// Crea un enrutador de Express
const router = Router();

// Rutas para las diferentes operaciones CRUD de Tesina
router.post('/crearTesina', crearTesina); // Ruta para crear un nuevo Tesina
router.get('/obtenerTesina', obtenerTesinas); // Ruta para obtener todos los Tesina
router.get('/obtenerTesina/:id', obtenerTesinaID); // Ruta para obtener un Tesina por su ID
router.delete('/eliminarTesina/:id', eliminarTesina); // Ruta para eliminar un Tesina por su ID
router.patch('/actualizarTesina/:id', actualizarTesina); // Ruta para actualizar un Tesina por su ID

//Rutas para las diferentes operaciones CRUD de los detalles de las Tesinas

// Exporta el enrutador
export default router;
