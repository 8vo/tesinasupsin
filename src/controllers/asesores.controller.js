/**
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024

 * Controladores para la API REST de gestión de asesores.
 * 
 * Este módulo contiene los controladores para manejar las operaciones CRUD
 * (Crear, Leer, Actualizar, Eliminar) relacionadas con los asesores.
 * 
 * @module Controladoresasesores
 */

// Importa el módulo de base de datos para ejecutar consultas SQL
import { pool } from "../db.js";
/**
 * Controlador para crear un nuevo asesores.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el nuevo asesores creado.
 */

export const crearasesores = async (req, res) => {
  // Extrae los datos del cuerpo de la solicitud
  const { idDocente, cargo, departamento } = req.body;

  // Inserta un nuevo registro de asesores en la base de datos
  const [rows] = await pool.query(
    "INSERT INTO asesores (ase_idDocente, ase_cargo, ase_departamento) VALUES (?, ?, ?);",
    [idDocente, cargo, departamento ]
  );

  // Envía una respuesta con el ID del nuevo asesores creado
  res.send({
    id: rows.insertId,
    cargo,
    departamento
  });
};

/**
 * Controlador para obtener todos los asesores.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con todos los asesores.
 */
export const obtenerasesores = async (req, res) => {
  // Obtiene todos los registros de asesores de la base de datos
  const [rows] = await pool.query("SELECT * FROM asesores;");

  // Envía una respuesta con los registros de asesores obtenidos
  res.send({
    rows,
  });
};

/**
 * Controlador para obtener un asesores por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el asesores encontrado o un mensaje de error si no se encuentra.
 */
export const obtenerasesoresID = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Busca un asesores por su ID de matrícula en la base de datos
  const [rows] = await pool.query("SELECT * FROM asesores WHERE ase_id = ?;", [
    id,
  ]);

  // Si no se encuentra ningún asesores, devuelve un mensaje de error
  if (rows.length <= 0)
    return res.status(404).json({
      message: "No se encontró nada",
    });

  // Envía una respuesta con el asesores encontrado
  res.send(rows[0]);
};

/**
 * Controlador para eliminar un asesores por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el resultado de la eliminación o un mensaje de error si no se puede eliminar.
 */
export const eliminarasesores = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Elimina un asesores por su ID de matrícula de la base de datos
  const [result] = await pool.query(
    "UPDATE asesores SET ase_idDocente = IFNULL(?, ase_idDocente), ase_cargo = IFNULL(?, ase_cargo), ase_departamento = IFNULL(?, ase_departamento) WHERE ase_id = ?",
    [ idDocente, cargo, departamento, id]
  );

  // Si no se puede eliminar ningún asesores, devuelve un mensaje de error
  if (result.affectedRows <= 0)
    return res.status(404).json({
      message: "Surgió un error al eliminar",
    });

  // Envía una respuesta indicando que el asesores fue eliminado con éxito
  res.sendStatus(204);
};

/**
 * Controlador para actualizar un asesores por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el asesores actualizado o un mensaje de error si no se puede actualizar.
 */
export const actualizarasesores = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const { id } = req.params;

  // Extrae los datos actualizados del cuerpo de la solicitud
  const {  idDocente, cargo, departamento } = req.body;

  // Actualiza un asesores por su ID de matrícula en la base de datos
  const [result] = await pool.query(
    "UPDATE asesores SET ase_idDocente = IFNULL(?, ase_idDocente), ase_cargo = IFNULL(?, ase_cargo), ase_departamento = IFNULL(?, ase_departamento) WHERE ase_id = ?",
    [ idDocente, cargo, departamento, id]
  );

  // Si no se puede actualizar ningún asesores, devuelve un mensaje de error
  if (result.affectedRows === 0)
    return res.status(404).json({ message: "Error al actualizar" });

  // Obtiene los datos del asesores actualizado de la base de datos
  const [rows] = await pool.query("SELECT * FROM asesores WHERE ase_id = ?", [
    id,
  ]);

  // Envía una respuesta con el asesores actualizado
  res.json(rows[0]);
};
