/**
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024

 * Controladores para la API REST de gestión de carreras.
 * 
 * Este módulo contiene los controladores para manejar las operaciones CRUD
 * (Crear, Leer, Actualizar, Eliminar) relacionadas con los carreras.
 * 
 * @module Controladorescarreras
 */

// Importa el módulo de base de datos para ejecutar consultas SQL
import { pool } from "../db.js";
/**
 * Controlador para crear un nuevo carreras.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el nuevo carreras creado.
 */

export const crearcarreras = async (req, res) => {
  // Extrae los datos del cuerpo de la solicitud
  const {
   titulo
  } = req.body;

  // Inserta un nuevo registro de carreras en la base de datos
  const [rows] = await pool.query(
    "INSERT INTO carreras (carrera) VALUES (?);",
    [
      titulo
    ]
  );

  // Envía una respuesta con el ID del nuevo carreras creado
  res.send({
    id: rows.insertId,
    titulo
  });
};

/**
 * Controlador para obtener todos los carreras.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con todos los carreras.
 */
export const obtenercarreras = async (req, res) => {
  // Obtiene todos los registros de carreras de la base de datos
  const [rows] = await pool.query("SELECT * FROM carreras;");

  // Envía una respuesta con los registros de carreras obtenidos
  res.send({
    rows,
  });
};

/**
 * Controlador para obtener un carreras por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el carreras encontrado o un mensaje de error si no se encuentra.
 */
export const obtenercarrerasID = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Busca un carreras por su ID de matrícula en la base de datos
  const [rows] = await pool.query(
    "SELECT * FROM carreras WHERE id = ?;",
    [id]
  );

  // Si no se encuentra ningún carreras, devuelve un mensaje de error
  if (rows.length <= 0)
    return res.status(404).json({
      message: "No se encontró nada",
    });

  // Envía una respuesta con el carreras encontrado
  res.send(rows[0]);
};

/**
 * Controlador para eliminar un carreras por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el resultado de la eliminación o un mensaje de error si no se puede eliminar.
 */
export const eliminarcarreras = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Elimina un carreras por su ID de matrícula de la base de datos
  const [result] = await pool.query(
    "DELETE FROM carreras WHERE id = ?;",
    [id]
  );

  // Si no se puede eliminar ningún carreras, devuelve un mensaje de error
  if (result.affectedRows <= 0)
    return res.status(404).json({
      message: "Surgió un error al eliminar",
    });

  // Envía una respuesta indicando que el carreras fue eliminado con éxito
  res.sendStatus(204);
};

/**
 * Controlador para actualizar un carreras por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el carreras actualizado o un mensaje de error si no se puede actualizar.
 */
export const actualizarcarreras = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const { id } = req.params;

  // Extrae los datos actualizados del cuerpo de la solicitud
  const {
    titulo
  } = req.body;

  // Actualiza un carreras por su ID de matrícula en la base de datos
  const [result] = await pool.query(
    "UPDATE carreras SET carrera = IFNULL(?, carrera)WHERE id = ?",
    [
      titulo, 
      id
    ]
  );

  // Si no se puede actualizar ningún carreras, devuelve un mensaje de error
  if (result.affectedRows === 0)
    return res.status(404).json({ message: "Error al actualizar" });

  // Obtiene los datos del carreras actualizado de la base de datos
  const [rows] = await pool.query(
    "SELECT * FROM carreras WHERE id = ?",
    [id]
  );

  // Envía una respuesta con el carreras actualizado
  res.json(rows[0]);
};
