/**
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024

 * Controladores para la API REST de gestión de Tesinas.
 * 
 * Este módulo contiene los controladores para manejar las operaciones CRUD
 * (Crear, Leer, Actualizar, Eliminar) relacionadas con los Tesinas.
 * 
 * @module ControladoresTesinas
 */

// Importa el módulo de base de datos para ejecutar consultas SQL
import { pool } from "../db.js";
/**
 * Controlador para crear un nuevo Tesina.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el nuevo Tesina creado.
 */

export const crearDetalleTesinas = async (req, res) => {
  // Extrae los datos del cuerpo de la solicitud
  const { idTesina, titulo, fechaCap, horaCap, caliAsesor, caliAsignaturaC1, caliAsignaturaC2, caliAsignaturaC3, caliEmpresario, url  } = req.body;

  // Inserta un nuevo registro de Tesina en la base de datos
  const [rows] = await pool.query(
    "INSERT INTO det_tesina (detTes_idTesina,detTes_titulo ,detTes_fechaCap,detTes_horaCap,detTes_caliAsesor,detTes_caliAsignaturaC1,detTes_caliAsignaturaC2,detTes_caliAsignaturaC3,detTes_caliEmpresario,detTes_url)VALUES(?,?,?,?,?,?,?,?,?,?);",
    [idTesina, titulo, fechaCap, horaCap, caliAsesor, caliAsignaturaC1, caliAsignaturaC2, caliAsignaturaC3, caliEmpresario, url]
  );

  // Envía una respuesta con el ID del nuevo Tesina creado
  res.send({
    id: rows.insertId,
    titulo
  });
};

/**
 * Controlador para obtener todos los Tesinas.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con todos los Tesinas.
 */
export const obtenerDetalleTesinas = async (req, res) => {
  // Obtiene todos los registros de Tesinas de la base de datos
  const [rows] = await pool.query("SELECT * FROM det_tesina;");

  // Envía una respuesta con los registros de Tesinas obtenidos
  res.send({
    rows,
  });
};

/**
 * Controlador para obtener un Tesina por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el Tesina encontrado o un mensaje de error si no se encuentra.
 */
export const obtenerDetalleTesinasID = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Busca un Tesina por su ID de matrícula en la base de datos
  const [rows] = await pool.query("SELECT * FROM det_tesina WHERE detTes_id = ?;", [
    id,
  ]);

  // Si no se encuentra ningún Tesina, devuelve un mensaje de error
  if (rows.length <= 0)
    return res.status(404).json({
      message: "No se encontró nada",
    });

  // Envía una respuesta con el Tesina encontrado
  res.send(rows[0]);
};

/**
 * Controlador para eliminar un Tesina por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el resultado de la eliminación o un mensaje de error si no se puede eliminar.
 */
export const eliminarDetalleTesinas = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Elimina un Tesina por su ID de matrícula de la base de datos
  const [result] = await pool.query("DELETE FROM det_tesina WHERE detTes_id = ?;", [
    id,
  ]);

  // Si no se puede eliminar ningún Tesina, devuelve un mensaje de error
  if (result.affectedRows <= 0)
    return res.status(404).json({
      message: "Surgió un error al eliminar",
    });

  // Envía una respuesta indicando que el Tesina fue eliminado con éxito
  res.sendStatus(204);
};

/**
 * Controlador para actualizar un Tesina por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el Tesina actualizado o un mensaje de error si no se puede actualizar.
 */
export const actualizarDetalleTesinas = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const { id } = req.params;

  // Extrae los datos actualizados del cuerpo de la solicitud
  const { idTesina, titulo, fechaCap, horaCap, caliAsesor, caliAsignaturaC1, caliAsignaturaC2, caliAsignaturaC3, caliEmpresario, url} = req.body;

  // Actualiza un Tesina por su ID de matrícula en la base de datos
  const [result] = await pool.query(
    "UPDATE det_tesina SET detTes_idTesina = IFNULL(?, detTes_idTesina), detTes_titulo = IFNULL(?, detTes_titulo), detTes_fechaCap = IFNULL(?, detTes_fechaCap), detTes_horaCap = IFNULL(?, detTes_horaCap), detTes_caliAsesor = IFNULL(?, detTes_caliAsesor), detTes_caliAsignaturaC1 = IFNULL(?, detTes_caliAsignaturaC1), detTes_caliAsignaturaC2 = IFNULL(?, detTes_caliAsignaturaC2), detTes_caliAsignaturaC3 = IFNULL(?, detTes_caliAsignaturaC3), detTes_caliEmpresario = IFNULL(?, detTes_caliEmpresario), detTes_url = IFNULL(?, detTes_id) WHERE detTes_id = ?;",
    [idTesina, titulo, fechaCap, horaCap, caliAsesor, caliAsignaturaC1, caliAsignaturaC2, caliAsignaturaC3, caliEmpresario, url, id]
  );

  // Si no se puede actualizar ningún Tesina, devuelve un mensaje de error
  if (result.affectedRows === 0)
    return res.status(404).json({ message: "Error al actualizar" });

  // Obtiene los datos del Tesina actualizado de la base de datos
  const [rows] = await pool.query(
    "SELECT * FROM det_tesina WHERE detTes_id = ?",
    [id]
  );

  // Envía una respuesta con el Tesina actualizado
  res.json(rows[0]);
};
