/**
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024

 * Controladores para la API REST de gestión de profesores.
 * 
 * Este módulo contiene los controladores para manejar las operaciones CRUD
 * (Crear, Leer, Actualizar, Eliminar) relacionadas con los profesores.
 * 
 * @module Controladoresprofesores
 */

// Importa el módulo de base de datos para ejecutar consultas SQL
import { pool } from "../db.js";
/**
 * Controlador para crear un nuevo profesores.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el nuevo profesores creado.
 */

export const crearprofesores = async (req, res) => {
  // Extrae los datos del cuerpo de la solicitud
  const { idDocente, cargo, departamento, asignatura, programa } = req.body;

  // Inserta un nuevo registro de profesores en la base de datos
  const [rows] = await pool.query(
    "INSERT INTO profesores (pro_idDocente, pro_cargo, pro_departamento, pro_asignatura, pro_programaAcademico) VALUES (?, ?, ?, ?, ?);",
    [idDocente, cargo, departamento, asignatura, programa]
  );

  // Envía una respuesta con el ID del nuevo profesores creado
  res.send({
    id: rows.insertId,
    cargo,
    departamento,
    asignatura,
    programa,
  });
};

/**
 * Controlador para obtener todos los profesores.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con todos los profesores.
 */
export const obtenerprofesores = async (req, res) => {
  // Obtiene todos los registros de profesores de la base de datos
  const [rows] = await pool.query("SELECT * FROM profesores;");

  // Envía una respuesta con los registros de profesores obtenidos
  res.send({
    rows,
  });
};

/**
 * Controlador para obtener un profesores por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el profesores encontrado o un mensaje de error si no se encuentra.
 */
export const obtenerprofesoresID = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Busca un profesores por su ID de matrícula en la base de datos
  const [rows] = await pool.query(
    "SELECT * FROM profesores WHERE pro_id = ?;",
    [id]
  );

  // Si no se encuentra ningún profesores, devuelve un mensaje de error
  if (rows.length <= 0)
    return res.status(404).json({
      message: "No se encontró nada",
    });

  // Envía una respuesta con el profesores encontrado
  res.send(rows[0]);
};

/**
 * Controlador para eliminar un profesores por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el resultado de la eliminación o un mensaje de error si no se puede eliminar.
 */
export const eliminarprofesores = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Elimina un alumno por su ID de matrícula de la base de datos
  const [result] = await pool.query(
    "DELETE FROM profesores WHERE pro_id = ?;",
    [id]
  );

  // Si no se puede eliminar ningún alumno, devuelve un mensaje de error
  if (result.affectedRows <= 0)
    return res.status(404).json({
      message: "Surgió un error al eliminar",
    });

  // Envía una respuesta indicando que el alumno fue eliminado con éxito
  res.sendStatus(204);
};

/**
 * Controlador para actualizar un profesores por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el profesores actualizado o un mensaje de error si no se puede actualizar.
 */
export const actualizarprofesores = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const { id } = req.params;

  // Extrae los datos actualizados del cuerpo de la solicitud
  const { idDocente, cargo, departamento, asignatura, programa } = req.body;

  // Actualiza un profesores por su ID de matrícula en la base de datos
  const [result] = await pool.query(
    "UPDATE profesores SET pro_idDocente = IFNULL(?, pro_idDocente), pro_cargo = IFNULL(?, pro_cargo), pro_departamento = IFNULL(?, pro_departamento), pro_asignatura = IFNULL(?, pro_asignatura), pro_programaAcademico = IFNULL(?, pro_programaAcademico)  WHERE pro_id = ?",
    [idDocente, cargo, departamento, asignatura, programa, id]
  );

  // Si no se puede actualizar ningún profesores, devuelve un mensaje de error
  if (result.affectedRows === 0)
    return res.status(404).json({ message: "Error al actualizar" });

  // Obtiene los datos del profesores actualizado de la base de datos
  const [rows] = await pool.query("SELECT * FROM profesores WHERE pro_id = ?", [
    id,
  ]);

  // Envía una respuesta con el profesores actualizado
  res.json(rows[0]);
};
