/**
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024

 * Controladores para la API REST de gestión de Docentes.
 * 
 * Este módulo contiene los controladores para manejar las operaciones CRUD
 * (Crear, Leer, Actualizar, Eliminar) relacionadas con los docentes.
 * 
 * @module ControladorDocente
 */

// Importa el módulo de base de datos para ejecutar consultas SQL
import { pool } from "../db.js";
/**
 * Controlador para crear un nuevo Docente.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el nuevo Docente creado.
 */

export const crearDocente = async (req, res) => {
  // Extrae los datos del cuerpo de la solicitud
  const {
    matricula,
    nombre,
    apellido,
    telefono,
    correoInstitucional,
    correoAux,
    telefonoAux,
  } = req.body;

  // Inserta un nuevo registro de Docentes en la base de datos
  const [rows] = await pool.query(
    "INSERT INTO docentes (doc_matricula, doc_nombre, doc_apellido, doc_telefonoPrincipal, doc_correoInstitucional, doc_correoAux, doc_telefonoAux) VALUES (?, ?, ?, ?, ?, ?, ?);",
    [
      matricula,
      nombre,
      apellido,
      telefono,
      correoInstitucional,
      correoAux,
      telefonoAux,
    ]
  );

  // Envía una respuesta con el ID del nuevo Docente creado
  res.send({
    id: rows.insertId,
    matricula,
    nombre,
  });
};

/**
 * Controlador para obtener todos los Docentes.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con todos los Docentes.
 */
export const obtenerDocentes = async (req, res) => {
  // Obtiene todos los registros de Docentes de la base de datos
  const [rows] = await pool.query("SELECT * FROM docentes;");

  // Envía una respuesta con los registros de Docentes obtenidos
  res.send({
    rows,
  });
};

/**
 * Controlador para obtener un Docente por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el Docente encontrado o un mensaje de error si no se encuentra.
 */
export const obtenerDocenteID = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Busca un Docente por su ID de matrícula en la base de datos
  const [rows] = await pool.query(
    "SELECT * FROM docentes WHERE doc_matricula = ?;",
    [id]
  );

  // Si no se encuentra ningún Docente, devuelve un mensaje de error
  if (rows.length <= 0)
    return res.status(404).json({
      message: "No se encontró nada",
    });

  // Envía una respuesta con el Docente encontrado
  res.send(rows[0]);
};

/**
 * Controlador para eliminar un Docente por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el resultado de la eliminación o un mensaje de error si no se puede eliminar.
 */
export const eliminarDocente = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Elimina un Docente por su ID de matrícula de la base de datos
  const [result] = await pool.query(
    "DELETE FROM docentes WHERE doc_matricula = ?;",
    [id]
  );

  // Si no se puede eliminar ningún Docente, devuelve un mensaje de error
  if (result.affectedRows <= 0)
    return res.status(404).json({
      message: "Surgió un error al eliminar",
    });

  // Envía una respuesta indicando que el Docente fue eliminado con éxito
  res.sendStatus(204);
};

/**
 * Controlador para actualizar un Docente por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el Docente actualizado o un mensaje de error si no se puede actualizar.
 */
export const actualizarDocente = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const { id } = req.params;

  // Extrae los datos actualizados del cuerpo de la solicitud
  const {
    matricula,
    nombre,
    apellido,
    telefono,
    correoInstitucional,
    correoAux,
    telefonoAux,
  } = req.body;


  console.log(req.body);

  // Actualiza un Docente por su ID de matrícula en la base de datos
  const [result] = await pool.query(
    "UPDATE docentes SET doc_matricula = IFNULL(?, doc_matricula), doc_nombre = IFNULL(?, doc_nombre), doc_apellido = IFNULL(?, doc_apellido), doc_telefonoPrincipal = IFNULL(?, doc_telefonoPrincipal), doc_correoInstitucional = IFNULL(?, doc_correoInstitucional), doc_correoAux = IFNULL(?, doc_correoAux), doc_telefonoAux = IFNULL(?, doc_telefonoAux) WHERE doc_matricula = ?",
    [
      matricula,
      nombre,
      apellido,
      telefono,
      correoInstitucional,
      correoAux,
      telefonoAux,
      id
    ]
  );

  console.log(result);

  // Si no se puede actualizar ningún Docente, devuelve un mensaje de error
  if (result.affectedRows === 0)
    return res.status(404).json({ message: "Error al actualizar" });

  // Obtiene los datos del Docente actualizado de la base de datos
  const [rows] = await pool.query(
    "SELECT * FROM docentes WHERE doc_matricula = ?",
    [id]
  );

  // Envía una respuesta con el Docente actualizado
  res.json(rows[0]);
};
