/**
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024

 * Controladores para la API REST de gestión de Tesinas.
 * 
 * Este módulo contiene los controladores para manejar las operaciones CRUD
 * (Crear, Leer, Actualizar, Eliminar) relacionadas con los Tesinas.
 * 
 * @module ControladoresTesinas
 */

// Importa el módulo de base de datos para ejecutar consultas SQL
import { pool } from "../db.js";
/**
 * Controlador para crear un nuevo Tesina.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el nuevo Tesina creado.
 */

export const crearTesina = async (req, res) => {
  // Extrae los datos del cuerpo de la solicitud
  const { idTesina, idAsesor, idProfesor, idDirector } = req.body;

  // Inserta un nuevo registro de Tesina en la base de datos
  const [rows] = await pool.query(
    "INSERT INTO tesina (tes_idTesina, tes_idAsesor, tes_idProfesor, tes_idDirector) VALUES (?, ?, ?, ?);",
    [idTesina, idAsesor, idProfesor, idDirector]
  );

  // Envía una respuesta con el ID del nuevo Tesina creado
  res.send({
    id: rows.insertId,
  });
};

/**
 * Controlador para obtener todos los Tesinas.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con todos los Tesinas.
 */
export const obtenerTesinas = async (req, res) => {
  // Obtiene todos los registros de Tesinas de la base de datos
  const [rows] = await pool.query("SELECT * FROM tesina;");

  // Envía una respuesta con los registros de Tesinas obtenidos
  res.send({
    rows,
  });
};

/**
 * Controlador para obtener un Tesina por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el Tesina encontrado o un mensaje de error si no se encuentra.
 */
export const obtenerTesinaID = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Busca un Tesina por su ID de matrícula en la base de datos
  const [rows] = await pool.query("SELECT * FROM tesina WHERE tes_id = ?;", [
    id,
  ]);

  // Si no se encuentra ningún Tesina, devuelve un mensaje de error
  if (rows.length <= 0)
    return res.status(404).json({
      message: "No se encontró nada",
    });

  // Envía una respuesta con el Tesina encontrado
  res.send(rows[0]);
};

/**
 * Controlador para eliminar un Tesina por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el resultado de la eliminación o un mensaje de error si no se puede eliminar.
 */
export const eliminarTesina = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Elimina un Tesina por su ID de matrícula de la base de datos
  const [result] = await pool.query("DELETE FROM tesina WHERE tes_id = ?;", [
    id,
  ]);

  // Si no se puede eliminar ningún Tesina, devuelve un mensaje de error
  if (result.affectedRows <= 0)
    return res.status(404).json({
      message: "Surgió un error al eliminar",
    });

  // Envía una respuesta indicando que el Tesina fue eliminado con éxito
  res.sendStatus(204);
};

/**
 * Controlador para actualizar un Tesina por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el Tesina actualizado o un mensaje de error si no se puede actualizar.
 */
export const actualizarTesina = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const { id } = req.params;

  // Extrae los datos actualizados del cuerpo de la solicitud
  const { idTesina, idAsesor, idProfesor, idDirector } = req.body;

  // Actualiza un Tesina por su ID de matrícula en la base de datos
  const [result] = await pool.query(
    "UPDATE tesina SET tes_idTesina = IFNULL(?, tes_idTesina), tes_idAsesor = IFNULL(?, tes_idAsesor), tes_idProfesor = IFNULL(?, tes_idProfesor), tes_idDirector = IFNULL(?, tes_idDirector) WHERE tes_id = ?",
    [idTesina, idAsesor, idProfesor, idDirector, id]
  );

  // Si no se puede actualizar ningún Tesina, devuelve un mensaje de error
  if (result.affectedRows === 0)
    return res.status(404).json({ message: "Error al actualizar" });

  // Obtiene los datos del Tesina actualizado de la base de datos
  const [rows] = await pool.query(
    "SELECT * FROM tesina WHERE tes_id = ?",
    [id]
  );

  // Envía una respuesta con el Tesina actualizado
  res.json(rows[0]);
};
