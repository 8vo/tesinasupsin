import { pool } from "../db.js";

export const ping = async (req, res) => {
  const [result] = await pool.query('SELECT "pong" AS result;');

  res.json(result[0]);
};

export const crearUsuario = async (req, res) => {
  const { nombre, edad } = req.body;
  const [rows] = await pool.query(
    "INSERT INTO pruebas (nombre, edad) VALUES (?, ?)",
    [nombre, edad]
  );

  res.send({
    id: rows.insertId,
    nombre,
    edad,
  });
};

export const obtenerUsuarios = async (req, res) => {
  const [rows] = await pool.query("SELECT * FROM pruebas;");

  res.send({
    rows,
  });
};

export const obtenerUsuariosID = async (req, res) => {
  const id = req.params.id;

  const [rows] = await pool.query("SELECT * FROM pruebas WHERE id = ?;", [id]);

  if (rows.length <= 0)
    return res.status(404).json({
      message: "No se encontro nada",
    });

  res.send(rows[0]);
};

export const eliminarUsuario = async (req, res) => {
  const id = req.params.id;

  const [result] = await pool.query("DELETE FROM pruebas WHERE id = ?;", [id]);

  if (result.affectedRows <= 0)
    return res.status(404).json({
      message: "Surgió un error al eliminar",
    });

  res.sendStatus(204);
};

export const actualizarUsuario = async (req, res) => {
  const { id } = req.params;
  const { nombre, edad } = req.body;

  const [result] = await pool.query(
    "UPDATE pruebas SET nombre = IFNULL(?, nombre), edad = IFNULL(?, edad) WHERE id = ?",
    [nombre, edad, id]
  );

  if (result.affectedRows === 0)
    return res.status(404).json({ message: "Error al actualizar" });


    const [rows] = await pool.query("SELECT * FROM pruebas WHERE id = ?", [id]);

    res.json(rows[0]);
};
