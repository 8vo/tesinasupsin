/**
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024

 * Controladores para la API REST de gestión de alumnos.
 * 
 * Este módulo contiene los controladores para manejar las operaciones CRUD
 * (Crear, Leer, Actualizar, Eliminar) relacionadas con los alumnos.
 * 
 * @module ControladoresAlumnos
 */

// Importa el módulo de base de datos para ejecutar consultas SQL
import { pool } from "../db.js";
/**
 * Controlador para crear un nuevo alumno.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el nuevo alumno creado.
 */

export const crearAlumno = async (req, res) => {
  // Extrae los datos del cuerpo de la solicitud
  const {
    matricula,
    nombre,
    apellido,
    telefono,
    fechaNacimiento,
    correoInstitucional,
    correoAux,
    telefonoAux,
    idCarrera,
  } = req.body;

  // Inserta un nuevo registro de alumno en la base de datos
  const [rows] = await pool.query(
    "INSERT INTO tesinas.alumnos (alu_matricula, alu_nombre, alu_apellido, alu_telefono, alu_fechaNacimiento, alu_correoInstitucional, alu_correoAux, alu_telefonoAux, alu_idCarrera) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);",
    [
      matricula,
      nombre,
      apellido,
      telefono,
      fechaNacimiento,
      correoInstitucional,
      correoAux,
      telefonoAux,
      idCarrera,
    ]
  );

  // Envía una respuesta con el ID del nuevo alumno creado
  res.send({
    id: rows.insertId,
    matricula,
    nombre,
    idCarrera,
  });
};

/**
 * Controlador para obtener todos los alumnos.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con todos los alumnos.
 */
export const obtenerAlumnos = async (req, res) => {
  // Obtiene todos los registros de alumnos de la base de datos
  const [rows] = await pool.query("SELECT * FROM alumnos;");

  // Envía una respuesta con los registros de alumnos obtenidos
  res.send({
    rows,
  });
};

/**
 * Controlador para obtener un alumno por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el alumno encontrado o un mensaje de error si no se encuentra.
 */
export const obtenerAlumnosID = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Busca un alumno por su ID de matrícula en la base de datos
  const [rows] = await pool.query(
    "SELECT * FROM alumnos WHERE alu_matricula = ?;",
    [id]
  );

  // Si no se encuentra ningún alumno, devuelve un mensaje de error
  if (rows.length <= 0)
    return res.status(404).json({
      message: "No se encontró nada",
    });

  // Envía una respuesta con el alumno encontrado
  res.send(rows[0]);
};

/**
 * Controlador para eliminar un alumno por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el resultado de la eliminación o un mensaje de error si no se puede eliminar.
 */
export const eliminarAlumnos = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Elimina un alumno por su ID de matrícula de la base de datos
  const [result] = await pool.query(
    "DELETE FROM alumnos WHERE alu_matricula = ?;",
    [id]
  );

  // Si no se puede eliminar ningún alumno, devuelve un mensaje de error
  if (result.affectedRows <= 0)
    return res.status(404).json({
      message: "Surgió un error al eliminar",
    });

  // Envía una respuesta indicando que el alumno fue eliminado con éxito
  res.sendStatus(204);
};

/**
 * Controlador para actualizar un alumno por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el alumno actualizado o un mensaje de error si no se puede actualizar.
 */
export const actualizarAlumnos = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const { id } = req.params;

  // Extrae los datos actualizados del cuerpo de la solicitud
  const {
    matricula,
    nombre,
    apellido,
    telefono,
    fechaNacimiento,
    correoInstitucional,
    correoAux,
    telefonoAux,
    idcarrera,
  } = req.body;

  // Actualiza un alumno por su ID de matrícula en la base de datos
  const [result] = await pool.query(
    "UPDATE alumnos SET alu_matricula = IFNULL(?, alu_matricula), alu_nombre = IFNULL(?, alu_nombre), alu_apellido = IFNULL(?, alu_apellido), alu_telefono = IFNULL(?, alu_telefono), alu_fechaNacimiento = IFNULL(?, alu_fechaNacimiento), alu_correoInstitucional = IFNULL(?, alu_correoInstitucional), alu_correoAux = IFNULL(?, alu_correoAux), alu_telefonoAux = IFNULL(?, alu_telefonoAux), alu_idcarrera = IFNULL(?, alu_idcarrera) WHERE alu_matricula = ?",
    [
      matricula,
      nombre,
      apellido,
      telefono,
      fechaNacimiento,
      correoInstitucional,
      correoAux,
      telefonoAux,
      idcarrera,
      id,
    ]
  );

  // Si no se puede actualizar ningún alumno, devuelve un mensaje de error
  if (result.affectedRows === 0)
    return res.status(404).json({ message: "Error al actualizar" });

  // Obtiene los datos del alumno actualizado de la base de datos
  const [rows] = await pool.query(
    "SELECT * FROM alumnos WHERE alu_matricula = ?",
    [id]
  );

  // Envía una respuesta con el alumno actualizado
  res.json(rows[0]);
};
