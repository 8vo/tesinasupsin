/**
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024

 * Controladores para la API REST de gestión de directores.
 * 
 * Este módulo contiene los controladores para manejar las operaciones CRUD
 * (Crear, Leer, Actualizar, Eliminar) relacionadas con los directores.
 * 
 * @module Controladoresdirectores
 */

// Importa el módulo de base de datos para ejecutar consultas SQL
import { pool } from "../db.js";
/**
 * Controlador para crear un nuevo directores.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el nuevo directores creado.
 */

export const creardirectores = async (req, res) => {
  // Extrae los datos del cuerpo de la solicitud
  const { idDocente, programa } = req.body;

  // Inserta un nuevo registro de directores en la base de datos
  const [rows] = await pool.query(
    "INSERT INTO directores (dire_idDocente, dire_programaAcademico) VALUES (?, ?);",
    [idDocente, programa]
  );

  // Envía una respuesta con el ID del nuevo directores creado
  res.send({
    id: rows.insertId,
    programa,
  });
};

/**
 * Controlador para obtener todos los directores.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con todos los directores.
 */
export const obtenerdirectores = async (req, res) => {
  // Obtiene todos los registros de directores de la base de datos
  const [rows] = await pool.query("SELECT * FROM directores;");

  // Envía una respuesta con los registros de directores obtenidos
  res.send({
    rows,
  });
};

/**
 * Controlador para obtener un directores por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el directores encontrado o un mensaje de error si no se encuentra.
 */
export const obtenerdirectoresID = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Busca un directores por su ID de matrícula en la base de datos
  const [rows] = await pool.query("SELECT * FROM directores WHERE dire_id = ?;", [
    id,
  ]);

  // Si no se encuentra ningún directores, devuelve un mensaje de error
  if (rows.length <= 0)
    return res.status(404).json({
      message: "No se encontró nada",
    });

  // Envía una respuesta con el directores encontrado
  res.send(rows[0]);
};

/**
 * Controlador para eliminar un directores por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el resultado de la eliminación o un mensaje de error si no se puede eliminar.
 */
export const eliminardirectores = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Elimina un directores por su ID de matrícula de la base de datos
  const [result] = await pool.query("DELETE FROM directores WHERE dire_id = ?;", [
    id,
  ]);

  // Si no se puede eliminar ningún directores, devuelve un mensaje de error
  if (result.affectedRows <= 0)
    return res.status(404).json({
      message: "Surgió un error al eliminar",
    });

  // Envía una respuesta indicando que el directores fue eliminado con éxito
  res.sendStatus(204);
};

/**
 * Controlador para actualizar un directores por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el directores actualizado o un mensaje de error si no se puede actualizar.
 */
export const actualizardirectores = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const { id } = req.params;

  // Extrae los datos actualizados del cuerpo de la solicitud
  const {  idDocente, programa } = req.body;

  // Actualiza un directores por su ID de matrícula en la base de datos
  const [result] = await pool.query(
    "UPDATE directores SET dire_idDocente = IFNULL(?, dire_idDocente), dire_programaAcademico = IFNULL(?, dire_programaAcademico) WHERE dire_id = ?",
    [ idDocente, programa, id]
  );

  // Si no se puede actualizar ningún directores, devuelve un mensaje de error
  if (result.affectedRows === 0)
    return res.status(404).json({ message: "Error al actualizar" });

  // Obtiene los datos del directores actualizado de la base de datos
  const [rows] = await pool.query("SELECT * FROM directores WHERE dire_id = ?", [
    id,
  ]);

  // Envía una respuesta con el directores actualizado
  res.json(rows[0]);
};
