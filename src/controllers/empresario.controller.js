/**
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024

 * Controladores para la API REST de gestión de Empresarios.
 * 
 * Este módulo contiene los controladores para manejar las operaciones CRUD
 * (Crear, Leer, Actualizar, Eliminar) relacionadas con los Empresarios.
 * 
 * @module ControladoresEmpresarios
 */

// Importa el módulo de base de datos para ejecutar consultas SQL
import { pool } from "../db.js";
/**
 * Controlador para crear un nuevo Empresario.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el nuevo Empresario creado.
 */

export const crearEmpresario = async (req, res) => {
  // Extrae los datos del cuerpo de la solicitud
  const {
    nombre,
    apellido,
    telefonoPrincipal,
    departamento,
    puesto,
    empresa,
    correoPrincipal,
    correoAux,
    telefonoAux,
  } = req.body;

  // Inserta un nuevo registro de Empresario en la base de datos
  const [rows] = await pool.query(
    "INSERT INTO empresarios (emp_nombre, emp_apellido, emp_telefono, emp_departamento, emp_cargo, emp_empresa, emp_correoPrincipal, emp_correoAux, emp_telefonoAux) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);",
    [
      nombre,
      apellido,
      telefonoPrincipal,
      departamento,
      puesto,
      empresa,
      correoPrincipal,
      correoAux,
      telefonoAux,
    ]
  );

  // Envía una respuesta con el ID del nuevo Empresario creado
  res.send({
    id: rows.insertId,
    nombre,
    empresa,
  });
};

/**
 * Controlador para obtener todos los Empresarios.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con todos los Empresarios.
 */
export const obtenerEmpresarios = async (req, res) => {
  // Obtiene todos los registros de Empresarios de la base de datos
  const [rows] = await pool.query("SELECT * FROM empresarios;");

  // Envía una respuesta con los registros de Empresarios obtenidos
  res.send({
    rows,
  });
};

/**
 * Controlador para obtener un Empresario por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el Empresario encontrado o un mensaje de error si no se encuentra.
 */
export const obtenerEmpresariosID = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Busca un Empresario por su ID de matrícula en la base de datos
  const [rows] = await pool.query(
    "SELECT * FROM empresarios WHERE emp_id = ?;",
    [id]
  );

  // Si no se encuentra ningún Empresario, devuelve un mensaje de error
  if (rows.length <= 0)
    return res.status(404).json({
      message: "No se encontró nada",
    });

  // Envía una respuesta con el Empresario encontrado
  res.send(rows[0]);
};

/**
 * Controlador para eliminar un Empresario por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el resultado de la eliminación o un mensaje de error si no se puede eliminar.
 */
export const eliminarEmpresarios = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const id = req.params.id;

  // Elimina un Empresario por su ID de matrícula de la base de datos
  const [result] = await pool.query(
    "DELETE FROM empresarios WHERE emp_id = ?;",
    [id]
  );

  // Si no se puede eliminar ningún Empresario, devuelve un mensaje de error
  if (result.affectedRows <= 0)
    return res.status(404).json({
      message: "Surgió un error al eliminar",
    });

  // Envía una respuesta indicando que el Empresario fue eliminado con éxito
  res.sendStatus(204);
};

/**
 * Controlador para actualizar un Empresario por su ID de matrícula.
 *
 * @param {object} req - El objeto de solicitud HTTP.
 * @param {object} res - El objeto de respuesta HTTP.
 * @returns {object} - Una respuesta HTTP con el Empresario actualizado o un mensaje de error si no se puede actualizar.
 */
export const actualizarEmpresarios = async (req, res) => {
  // Obtiene el ID de matrícula del parámetro de la solicitud
  const { id } = req.params;

  // Extrae los datos actualizados del cuerpo de la solicitud
  const {
    nombre,
    apellido,
    telefonoPrincipal,
    departamento,
    puesto,
    empresa,
    correoPrincipal,
    correoAux,
    telefonoAux,
  } = req.body;
  console.log(req.query);

  // Actualiza un Empresario por su ID de matrícula en la base de datos
  const [result] = await pool.query(
    "UPDATE empresarios SET emp_nombre = IFNULL(?, emp_nombre), emp_apellido = IFNULL(?, emp_apellido), emp_telefono = IFNULL(?, emp_telefono), emp_departamento = IFNULL(?, emp_departamento), emp_cargo = IFNULL(?, emp_cargo), emp_empresa = IFNULL(?, emp_empresa), emp_correoPrincipal = IFNULL(?, emp_correoPrincipal), emp_telefonoAux = IFNULL(?, emp_telefonoAux) WHERE emp_id = ?;",
    [
      nombre,
      apellido,
      telefonoPrincipal,
      departamento,
      puesto,
      empresa,
      correoPrincipal,
      correoAux,
      telefonoAux,
      id,
    ]
  );

  console.log(result);
  // Si no se puede actualizar ningún Empresario, devuelve un mensaje de error
  if (result.affectedRows === 0)
    return res.status(404).json({ message: "Error al actualizar" });

  // Obtiene los datos del Empresario actualizado de la base de datos
  const [rows] = await pool.query(
    "SELECT * FROM empresarios WHERE emp_id = ?",
    [id]
  );

  // Envía una respuesta con el Empresario actualizado
  res.json(rows[0]);
};
