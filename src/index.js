/**
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024
 * 
 * Módulo principal de la aplicación de servidor.
 * 
 * Este módulo configura y ejecuta un servidor Express que maneja las solicitudes HTTP para una API REST.
 * Utiliza diferentes rutas para manejar las solicitudes relacionadas con diferentes recursos, como alumnos, empresarios y docentes.
 * 
 * @module Servidor
 */

// Importa el módulo Express para crear y configurar el servidor
import express from "express";

// Importa las rutas para diferentes recursos
import indexRoutes from "./routes/index.routes.js";
import alumnosRoutes from "./routes/alumnos.routes.js";
import empresariosRoutes from "./routes/empresarios.routes.js";
import docenteRoutes from "./routes/docente.routes.js";
import tesinasRoutes from "./routes/tesinas.routes.js"
import carrerasTesinas from "./routes/carreras.routes.js"
import directoresRoutes from "./routes/directores.routes.js"
import asesoresRoutes from "./routes/asesores.routes.js"
import profesorRoutes from "./routes/profesor.routes.js"
import detallesTesinas from "./routes/detallesTesinas.routes.js"

// Crea una instancia de la aplicación Express
const app = express();

// Middleware para parsear el cuerpo de las solicitudes HTTP como JSON
app.use(express.json());

// Configura las rutas para diferentes recursos
app.use('/api', indexRoutes); // Ruta raíz de la API
app.use('/alumnos', alumnosRoutes); // Rutas relacionadas con los alumnos
app.use('/empresarios', empresariosRoutes); // Rutas relacionadas con los empresarios
app.use('/docentes', docenteRoutes); // Rutas relacionadas con los docentes
app.use('/carreras', carrerasTesinas); // Rutas relacionadas con las carreras
app.use('/tesinas', tesinasRoutes); // Rutas relacionadas con las tesinas
app.use('/directores', directoresRoutes); // Rutas relacionadas con los Directores
app.use('/asesores', asesoresRoutes); // Rutas relacionadas con los asesores
app.use('/profesor', profesorRoutes); // Rutas relacionadas con con los profesores
app.use('/detTesinas', detallesTesinas); // Rutas relacionadas con los detalles de cada tesina

// Inicia el servidor Express y lo escucha en el puerto 3000
app.listen(3000);
console.log("Server corriendo en el puerto 3000");
