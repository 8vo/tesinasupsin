/**
 * @author Ángel Antonio Ibarra Hernández
 * @copyright Universidad Politécnica de Sinaloa - 2024
 * 
 * Módulo de configuración de la conexión a la base de datos MySQL.
 * 
 * Este módulo exporta una instancia de conexión a la base de datos MySQL utilizando la librería mysql2/promise.
 * 
 * @module ConfiguracionBaseDatos
 */

// Importa la función createPool de la librería mysql2/promise para crear un pool de conexiones a la base de datos MySQL
import { createPool } from 'mysql2/promise';

// Crea un pool de conexiones a la base de datos MySQL con la configuración especificada
export const pool = createPool({
    host: 'localhost', // Dirección del servidor de base de datos MySQL
    user: 'root', // Nombre de usuario para acceder a la base de datos
    password: 'cisco123', // Contraseña para acceder a la base de datos
    port: 3306, // Puerto en el que se encuentra el servidor de base de datos MySQL
    database: 'tesinas' // Nombre de la base de datos a la que se quiere conectar
});
