-- /*
-- SQLyog Ultimate v13.1.1 (64 bit)
-- MySQL - 10.4.28-MariaDB : Database - tesinas
-- *********************************************************************
-- */

-- /*!40101 SET NAMES utf8 */;

-- /*!40101 SET SQL_MODE=''*/;

-- /*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
-- /*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
-- /*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- /*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
-- CREATE DATABASE /*!32312 IF NOT EXISTS*/`tesinas` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;

-- USE `tesinas`;

-- /*Table structure for table `alumno_correos` */

-- DROP TABLE IF EXISTS `alumno_correos`;

-- CREATE TABLE `alumno_correos` (
--   `alCo_matricula` INT(11) NOT NULL,
--   `alCo_correoInstitucional` VARCHAR(25) NOT NULL,
--   `alCo_correoPersonal` VARCHAR(25) NOT NULL,
--   PRIMARY KEY (`alCo_matricula`)
-- ) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `alumno_correos` */

-- /*Table structure for table `alumno_telefonos` */

-- DROP TABLE IF EXISTS `alumno_telefonos`;

-- CREATE TABLE `alumno_telefonos` (
--   `alTel_matricula` int(11) NOT NULL,
--   `alTel_telefonoPersonal` varchar(11) NOT NULL,
--   `alTel_telefonoCasa` varchar(11) NOT NULL,
--   `alTel_telefonoFamiliar` varchar(11) NOT NULL,
--   PRIMARY KEY (`alTel_matricula`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `alumno_telefonos` */

-- /*Table structure for table `alumnos` */

-- DROP TABLE IF EXISTS `alumnos`;

-- CREATE TABLE `alumnos` (
--   `alu_matricula` int(11) NOT NULL,
--   `alu_nombre` varchar(35) NOT NULL,
--   `alu_apellido` varchar(35) NOT NULL,
--   `alu_telefono` varchar(10) NOT NULL,
--   `alu_fechaNacimiento` date NOT NULL,
--   `alu_idCorreo` int(11) NOT NULL,
--   `alu_idTelefono` int(11) NOT NULL,
--   PRIMARY KEY (`alu_matricula`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `alumnos` */

-- /*Table structure for table `asesores` */

-- DROP TABLE IF EXISTS `asesores`;

-- CREATE TABLE `asesores` (
--   `ase_id` int(11) NOT NULL AUTO_INCREMENT,
--   `ase_idDocente` int(11) NOT NULL,
--   `ase_cargo` varchar(50) DEFAULT NULL,
--   `ase_despartamento` varchar(50) DEFAULT NULL,
--   PRIMARY KEY (`ase_id`),
--   KEY `ase_idDocente` (`ase_idDocente`),
--   CONSTRAINT `asesores_ibfk_1` FOREIGN KEY (`ase_idDocente`) REFERENCES `docentes` (`doc_matricula`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `asesores` */

-- /*Table structure for table `det_tesina` */

-- DROP TABLE IF EXISTS `det_tesina`;

-- CREATE TABLE `det_tesina` (
--   `detTes_id` int(11) NOT NULL AUTO_INCREMENT,
--   `detTes_idTesina` int(11) NOT NULL,
--   `detTes_titulo` varchar(100) NOT NULL,
--   `detTes_fechaCap` date NOT NULL,
--   `detTes_horaCap` time NOT NULL,
--   `detTes_caliAsesor` float NOT NULL,
--   `detTes_caliAsignaturaC1` float NOT NULL,
--   `detTes_caliAsignaturaC2` float NOT NULL,
--   `detTes_caliAsignaturaC3` float NOT NULL,
--   `detTes_caliEmpresario` float NOT NULL,
--   `detTes_url` varchar(250) NOT NULL,
--   PRIMARY KEY (`detTes_id`),
--   KEY `detTes_idTesina` (`detTes_idTesina`),
--   CONSTRAINT `det_tesina_ibfk_1` FOREIGN KEY (`detTes_idTesina`) REFERENCES `tesina` (`tes_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `det_tesina` */

-- /*Table structure for table `directores` */

-- DROP TABLE IF EXISTS `directores`;

-- CREATE TABLE `directores` (
--   `dire_id` int(11) NOT NULL AUTO_INCREMENT,
--   `dire_idDocente` int(11) NOT NULL,
--   `dire_programaAcademico` varchar(50) DEFAULT NULL,
--   PRIMARY KEY (`dire_id`),
--   KEY `dire_idDocente` (`dire_idDocente`),
--   CONSTRAINT `directores_ibfk_1` FOREIGN KEY (`dire_idDocente`) REFERENCES `docentes` (`doc_matricula`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `directores` */

-- /*Table structure for table `docente_correos` */

-- DROP TABLE IF EXISTS `docente_correos`;

-- CREATE TABLE `docente_correos` (
--   `docCo_matricula` int(11) NOT NULL,
--   `docCo_correoInstitucional` varchar(25) NOT NULL,
--   `docCo_correoPersonal` varchar(25) DEFAULT NULL,
--   PRIMARY KEY (`docCo_matricula`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `docente_correos` */

-- /*Table structure for table `docente_telefonos` */

-- DROP TABLE IF EXISTS `docente_telefonos`;

-- CREATE TABLE `docente_telefonos` (
--   `alTel_matricula` int(11) NOT NULL,
--   `alTel_telefono1` varchar(11) DEFAULT NULL,
--   `alTel_telefono2` varchar(11) DEFAULT NULL,
--   `alTel_telefono3` varchar(11) DEFAULT NULL,
--   PRIMARY KEY (`alTel_matricula`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `docente_telefonos` */

-- /*Table structure for table `docentes` */

-- DROP TABLE IF EXISTS `docentes`;

-- CREATE TABLE `docentes` (
--   `doc_matricula` int(11) NOT NULL,
--   `doc_nombre` varchar(35) NOT NULL,
--   `doc_apellido` varchar(35) NOT NULL,
--   `doc_telefono` varchar(10) NOT NULL,
--   `doc_idCorreo` int(11) NOT NULL,
--   `doc_idTelefono` int(11) NOT NULL,
--   PRIMARY KEY (`doc_matricula`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `docentes` */

-- /*Table structure for table `documentos_extra` */

-- DROP TABLE IF EXISTS `documentos_extra`;

-- CREATE TABLE `documentos_extra` (
--   `docuE_id` int(11) NOT NULL AUTO_INCREMENT,
--   `docuE_titulo` varchar(100) NOT NULL,
--   `docuE_fechaCarga` date NOT NULL,
--   `docuE_url` varchar(250) NOT NULL,
--   PRIMARY KEY (`docuE_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `documentos_extra` */

-- /*Table structure for table `empresarios` */

-- DROP TABLE IF EXISTS `empresarios`;

-- CREATE TABLE `empresarios` (
--   `emp_id` int(11) NOT NULL,
--   `emp_nombre` varchar(35) NOT NULL,
--   `emp_apellido` varchar(35) NOT NULL,
--   `emp_telefono` varchar(10) NOT NULL,
--   `emp_departamento` varchar(35) NOT NULL,
--   `emp_cargo` varchar(35) NOT NULL,
--   `emp_empresa` varchar(100) NOT NULL,
--   `emp_idCorreo` int(11) NOT NULL,
--   `emp_idTelefono` int(11) NOT NULL,
--   PRIMARY KEY (`emp_id`),
--   KEY `emp_idCorreo` (`emp_idCorreo`),
--   KEY `emp_idTelefono` (`emp_idTelefono`),
--   CONSTRAINT `empresarios_ibfk_1` FOREIGN KEY (`emp_idCorreo`) REFERENCES `empresarios_correos` (`empCo_id`),
--   CONSTRAINT `empresarios_ibfk_2` FOREIGN KEY (`emp_idTelefono`) REFERENCES `empresarios_telefonos` (`empTel_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `empresarios` */

-- /*Table structure for table `empresarios_correos` */

-- DROP TABLE IF EXISTS `empresarios_correos`;

-- CREATE TABLE `empresarios_correos` (
--   `empCo_id` int(11) NOT NULL,
--   `empCo_correoInstitucional` varchar(25) NOT NULL,
--   `empCo_correoPersonal` varchar(25) DEFAULT NULL,
--   PRIMARY KEY (`empCo_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `empresarios_correos` */

-- /*Table structure for table `empresarios_telefonos` */

-- DROP TABLE IF EXISTS `empresarios_telefonos`;

-- CREATE TABLE `empresarios_telefonos` (
--   `empTel_id` int(11) NOT NULL,
--   `ampTel_telefono1` varchar(11) DEFAULT NULL,
--   `empTel_telefono2` varchar(11) DEFAULT NULL,
--   `empTel_telefono3` varchar(11) DEFAULT NULL,
--   PRIMARY KEY (`empTel_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `empresarios_telefonos` */

-- /*Table structure for table `profesores` */

-- DROP TABLE IF EXISTS `profesores`;

-- CREATE TABLE `profesores` (
--   `pro_id` int(11) NOT NULL AUTO_INCREMENT,
--   `pro_idDocente` int(11) NOT NULL,
--   `pro_cargo` varchar(50) DEFAULT NULL,
--   `pro_despartamento` varchar(50) DEFAULT NULL,
--   `pro_asignatura` varchar(35) DEFAULT NULL,
--   `pro_programaAcademico` varchar(50) DEFAULT NULL,
--   PRIMARY KEY (`pro_id`),
--   KEY `pro_idDocente` (`pro_idDocente`),
--   CONSTRAINT `profesores_ibfk_1` FOREIGN KEY (`pro_idDocente`) REFERENCES `docentes` (`doc_matricula`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `profesores` */

-- /*Table structure for table `tesina` */

-- DROP TABLE IF EXISTS `tesina`;

-- CREATE TABLE `tesina` (
--   `tes_id` int(11) NOT NULL AUTO_INCREMENT,
--   `tes_idAlumno` int(11) NOT NULL,
--   `tes_idAsesor` int(11) NOT NULL,
--   `tes_idProfesor` int(11) NOT NULL,
--   `tes_idEmpresario` int(11) NOT NULL,
--   `tes_idDirector` int(11) NOT NULL,
--   PRIMARY KEY (`tes_id`),
--   KEY `tes_idAlumno` (`tes_idAlumno`),
--   KEY `tes_idAsesor` (`tes_idAsesor`),
--   KEY `tes_idProfesor` (`tes_idProfesor`),
--   KEY `tes_idEmpresario` (`tes_idEmpresario`),
--   KEY `tes_idDirector` (`tes_idDirector`),
--   CONSTRAINT `tesina_ibfk_1` FOREIGN KEY (`tes_idAlumno`) REFERENCES `alumnos` (`alu_matricula`),
--   CONSTRAINT `tesina_ibfk_2` FOREIGN KEY (`tes_idAsesor`) REFERENCES `asesores` (`ase_id`),
--   CONSTRAINT `tesina_ibfk_3` FOREIGN KEY (`tes_idProfesor`) REFERENCES `profesores` (`pro_id`),
--   CONSTRAINT `tesina_ibfk_4` FOREIGN KEY (`tes_idEmpresario`) REFERENCES `empresarios` (`emp_id`),
--   CONSTRAINT `tesina_ibfk_5` FOREIGN KEY (`tes_idDirector`) REFERENCES `directores` (`dire_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `tesina` */

-- /*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
-- /*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
-- /*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
-- /*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
