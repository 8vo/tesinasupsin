-- /*
-- SQLyog Ultimate v13.1.1 (64 bit)
-- MySQL - 10.4.28-MariaDB : Database - tesinas
-- *********************************************************************
-- */

-- /*!40101 SET NAMES utf8 */;

-- /*!40101 SET SQL_MODE=''*/;

-- /*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
-- /*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
-- /*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- /*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
-- CREATE DATABASE /*!32312 IF NOT EXISTS*/`tesinas` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;

-- USE `tesinas`;

-- /*Table structure for table `alumnos` */

-- DROP TABLE IF EXISTS `alumnos`;

-- CREATE TABLE `alumnos` (
--   `alu_matricula` int(11) NOT NULL,
--   `alu_nombre` varchar(35) NOT NULL,
--   `alu_apellido` varchar(35) NOT NULL,
--   `alu_telefono` varchar(11) NOT NULL,
--   `alu_fechaNacimiento` date NOT NULL,
--   `alu_correoInstitucional` varchar(100) NOT NULL,
--   `alu_correoAux` varchar(100) DEFAULT NULL,
--   `alu_telefonoAux` varchar(11) DEFAULT NULL,
--   `alu_idcarrera` int(11) NOT NULL,
--   PRIMARY KEY (`alu_matricula`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `alumnos` */

-- insert  into `alumnos`(`alu_matricula`,`alu_nombre`,`alu_apellido`,`alu_telefono`,`alu_fechaNacimiento`,`alu_correoInstitucional`,`alu_correoAux`,`alu_telefonoAux`,`alu_idcarrera`) values 
-- (0,'Angel Antonio','Ibarra Hernandez','6692363920','2003-01-21','2021030118@upsin.edu.mx','Angelibarra671@gmail.com','6691003798',1);

-- /*Table structure for table `asesores` */

-- DROP TABLE IF EXISTS `asesores`;

-- CREATE TABLE `asesores` (
--   `ase_id` int(11) NOT NULL AUTO_INCREMENT,
--   `ase_idDocente` int(11) NOT NULL,
--   `ase_cargo` varchar(50) DEFAULT NULL,
--   `ase_despartamento` varchar(50) DEFAULT NULL,
--   PRIMARY KEY (`ase_id`),
--   KEY `ase_idDocente` (`ase_idDocente`),
--   CONSTRAINT `asesores_ibfk_1` FOREIGN KEY (`ase_idDocente`) REFERENCES `docentes` (`doc_matricula`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `asesores` */

-- /*Table structure for table `carreras` */

-- DROP TABLE IF EXISTS `carreras`;

-- CREATE TABLE `carreras` (
--   `id` int(11) NOT NULL AUTO_INCREMENT,
--   `carreRa` varchar(50) NOT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `carreras` */

-- insert  into `carreras`(`id`,`carreRa`) values 
-- (1,'Ing. Tecnologías de la Información'),
-- (2,'Ing. Mecatronica'),
-- (3,'Ing. Bomédica'),
-- (4,'Ing. Logistica y Transporte'),
-- (5,'Ing. Biotecnología'),
-- (6,'Ing. Nanotecnología'),
-- (7,'Lic. Administración de Empresas'),
-- (8,'Lic. Terapia Física'),
-- (9,'Ing. Energía'),
-- (10,'Ing. Tecnología Ambiental'),
-- (11,'Ing. Animación y Efectos Visuales'),
-- (12,'Maestría en Cencias Aplicadas'),
-- (13,'Maestría en Enseñanza de las Ciencias');

-- /*Table structure for table `det_tesina` */

-- DROP TABLE IF EXISTS `det_tesina`;

-- CREATE TABLE `det_tesina` (
--   `detTes_id` int(11) NOT NULL AUTO_INCREMENT,
--   `detTes_idTesina` int(11) NOT NULL,
--   `detTes_titulo` varchar(100) NOT NULL,
--   `detTes_fechaCap` date NOT NULL,
--   `detTes_horaCap` time NOT NULL,
--   `detTes_caliAsesor` float NOT NULL,
--   `detTes_caliAsignaturaC1` float NOT NULL,
--   `detTes_caliAsignaturaC2` float NOT NULL,
--   `detTes_caliAsignaturaC3` float NOT NULL,
--   `detTes_caliEmpresario` float NOT NULL,
--   `detTes_url` varchar(250) NOT NULL,
--   PRIMARY KEY (`detTes_id`),
--   KEY `detTes_idTesina` (`detTes_idTesina`),
--   CONSTRAINT `det_tesina_ibfk_1` FOREIGN KEY (`detTes_idTesina`) REFERENCES `tesina` (`tes_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `det_tesina` */

-- /*Table structure for table `directores` */

-- DROP TABLE IF EXISTS `directores`;

-- CREATE TABLE `directores` (
--   `dire_id` int(11) NOT NULL AUTO_INCREMENT,
--   `dire_idDocente` int(11) NOT NULL,
--   `dire_programaAcademico` varchar(50) DEFAULT NULL,
--   PRIMARY KEY (`dire_id`),
--   KEY `dire_idDocente` (`dire_idDocente`),
--   CONSTRAINT `directores_ibfk_1` FOREIGN KEY (`dire_idDocente`) REFERENCES `docentes` (`doc_matricula`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `directores` */

-- /*Table structure for table `docentes` */

-- DROP TABLE IF EXISTS `docentes`;

-- CREATE TABLE `docentes` (
--   `doc_matricula` int(11) NOT NULL,
--   `doc_nombre` varchar(35) NOT NULL,
--   `doc_apellido` varchar(35) NOT NULL,
--   `doc_telefonoPrincipal` varchar(11) NOT NULL,
--   `doc_telefonoAux` varchar(11) DEFAULT NULL,
--   `doc_correoInstitucional` varchar(100) NOT NULL,
--   `doc_correoAux` varchar(100) DEFAULT NULL,
--   PRIMARY KEY (`doc_matricula`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `docentes` */

-- insert  into `docentes`(`doc_matricula`,`doc_nombre`,`doc_apellido`,`doc_telefonoPrincipal`,`doc_telefonoAux`,`doc_correoInstitucional`,`doc_correoAux`) values 
-- (2021030117,'Pedro Pablo','Gonzaléz Peréz','6692363920','6691003798','pGonzalez@upsin.edu.mx','angelibarra671@gmail.com'),
-- (2021030118,'Juan','Rojas','6692363920','6691003798','pGonzalez@upsin.edu.mx','angelibarra671@gmail.com');

-- /*Table structure for table `documentos_extra` */

-- DROP TABLE IF EXISTS `documentos_extra`;

-- CREATE TABLE `documentos_extra` (
--   `docuE_id` int(11) NOT NULL AUTO_INCREMENT,
--   `docuE_titulo` varchar(100) NOT NULL,
--   `docuE_fechaCarga` date NOT NULL,
--   `docuE_url` varchar(250) NOT NULL,
--   PRIMARY KEY (`docuE_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `documentos_extra` */

-- /*Table structure for table `empresarios` */

-- DROP TABLE IF EXISTS `empresarios`;

-- CREATE TABLE `empresarios` (
--   `emp_id` int(11) NOT NULL AUTO_INCREMENT,
--   `emp_nombre` varchar(35) NOT NULL,
--   `emp_apellido` varchar(35) NOT NULL,
--   `emp_telefono` varchar(10) NOT NULL,
--   `emp_departamento` varchar(35) NOT NULL,
--   `emp_cargo` varchar(35) NOT NULL,
--   `emp_empresa` varchar(100) NOT NULL,
--   `emp_correoPrincipal` varchar(100) NOT NULL,
--   `emp_correoAux` varchar(100) DEFAULT NULL,
--   `emp_telefonoAux` varchar(12) DEFAULT NULL,
--   PRIMARY KEY (`emp_id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `empresarios` */

-- insert  into `empresarios`(`emp_id`,`emp_nombre`,`emp_apellido`,`emp_telefono`,`emp_departamento`,`emp_cargo`,`emp_empresa`,`emp_correoPrincipal`,`emp_correoAux`,`emp_telefonoAux`) values 
-- (1,'Ángel','ibarra','6692363920','Jefe de ST','Encargado','Farmaclinik','ibm@farmaclinik.mx','angelibarra671@gmai.com','6691003798'),
-- (2,'Ángel','ibarra','6692363920','Jefe de ST','Encargado','Farmaclinik','ibm@farmaclinik.mx','angelibarra671@gmai.com','6691003798'),
-- (3,'Ángel','ibarra','6692363920','Jefe de ST','Encargado','Farmaclinik','ibm@farmaclinik.mx','angelibarra671@gmai.com','6691003798');

-- /*Table structure for table `profesores` */

-- DROP TABLE IF EXISTS `profesores`;

-- CREATE TABLE `profesores` (
--   `pro_id` int(11) NOT NULL AUTO_INCREMENT,
--   `pro_idDocente` int(11) NOT NULL,
--   `pro_cargo` varchar(50) DEFAULT NULL,
--   `pro_despartamento` varchar(50) DEFAULT NULL,
--   `pro_asignatura` varchar(35) DEFAULT NULL,
--   `pro_programaAcademico` varchar(50) DEFAULT NULL,
--   PRIMARY KEY (`pro_id`),
--   KEY `pro_idDocente` (`pro_idDocente`),
--   CONSTRAINT `profesores_ibfk_1` FOREIGN KEY (`pro_idDocente`) REFERENCES `docentes` (`doc_matricula`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `profesores` */

-- /*Table structure for table `pruebas` */

-- DROP TABLE IF EXISTS `pruebas`;

-- CREATE TABLE `pruebas` (
--   `id` int(11) NOT NULL AUTO_INCREMENT,
--   `nombre` varchar(30) NOT NULL,
--   `edad` varchar(3) DEFAULT NULL,
--   PRIMARY KEY (`id`)
-- ) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `pruebas` */

-- insert  into `pruebas`(`id`,`nombre`,`edad`) values 
-- (7,'Angel','00'),
-- (8,'Angel','00'),
-- (10,'Pedro Juan','21');

-- /*Table structure for table `tesina` */

-- DROP TABLE IF EXISTS `tesina`;

-- CREATE TABLE `tesina` (
--   `tes_id` int(11) NOT NULL AUTO_INCREMENT,
--   `tes_idAlumno` int(11) NOT NULL,
--   `tes_idAsesor` int(11) NOT NULL,
--   `tes_idProfesor` int(11) NOT NULL,
--   `tes_idDirector` int(11) NOT NULL,
--   PRIMARY KEY (`tes_id`),
--   KEY `tes_idAlumno` (`tes_idAlumno`),
--   KEY `tes_idAsesor` (`tes_idAsesor`),
--   KEY `tes_idProfesor` (`tes_idProfesor`),
--   KEY `tes_idDirector` (`tes_idDirector`),
--   CONSTRAINT `tesina_ibfk_1` FOREIGN KEY (`tes_idAlumno`) REFERENCES `alumnos` (`alu_matricula`),
--   CONSTRAINT `tesina_ibfk_2` FOREIGN KEY (`tes_idAsesor`) REFERENCES `asesores` (`ase_id`),
--   CONSTRAINT `tesina_ibfk_3` FOREIGN KEY (`tes_idProfesor`) REFERENCES `profesores` (`pro_id`),
--   CONSTRAINT `tesina_ibfk_5` FOREIGN KEY (`tes_idDirector`) REFERENCES `directores` (`dire_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- /*Data for the table `tesina` */

-- /*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
-- /*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
-- /*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
-- /*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
